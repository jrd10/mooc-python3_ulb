# MOOC-Python3_ulb

Bonjour,

L'hébergement de mes exercices et codes du projet du MOOC :
Apprendre à coder avec Python, de l'Université Libre de Bruxelles et de Université de la Réunion.

Vous pouvez rejoindre le projet, vos issues et pull request seront les bienvenus pour initiation et amélioration du code.

Note : C'est mon premier projet sur GitLab :)

Le projet est sous la licence "**The Unlicense**" pour mes productions. Sauf mention expresse, vos commentaires seront placées sous cette licence.

Pour la licence du MOOC lui-même, cours, exos, projets, se reporter à la licence retenue par les auteurs.


A bientôt.

[Lien vers le MOOC](https://www.fun-mooc.fr/courses/course-v1:ulb+44013+session03/courseware/dffbd36f242f44c2b8bcb83ba4aa4eba/).


Début, le 10/02/2020.
Pour une période de 15 semaines.

Amicalement, 
José Relland
Membre de Tricassinux.org
